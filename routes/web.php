<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CommentController;



Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return view('greeting', ['name' => $user = Auth::user()->name]);
} )->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news', 'UserController@index')->name('news');

Route::get('/news', 'ArticleController@index')->name('news');

Route::get('profile', 'UserController@profile')->middleware('auth');

Route::resource('posts','PostController')->middleware('auth');

Route::resource('comments', 'CommentController')->middleware('auth');

Route::post('profile', 'UserController@update_avatar')->middleware('auth');

Route::post('/image/upload', 'UserController@store')->name('upload-image');

Route::get('users/{edit}', 'UserController@edit')->name('users.edit');

Route::put('users/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);

// Route::get('/posts')

Auth::routes();
