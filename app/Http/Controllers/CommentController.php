<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     * 
     * @param \illuminate\Http\Request  $request
     * @return \illuminate\Http\Response
     */

     public function store(Request $request)
     {
         $request->validate([
             'body'=>'required',
         ]);

         $input = $request->all();
         $input['user_id'] = auth()->user()->id;

         Comment::create($input);

         return back();
     }
}
