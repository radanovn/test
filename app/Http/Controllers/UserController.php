<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();

        return view('profile', compact('user', $user));
    }



    //Hasing function for the image name
    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        //Check if image is presented in the request and it's valid
        if ($request->hasFile('avatar') && $request->avatar->isValid()) {
            $image = $request->avatar;
            $image_name = $this->hashImageName($image);

            //store the image in the filesystem
            Storage::disk('images')->putFileAs('', $image, $image_name);

            Storage::disk('images')->delete($user->avatar);

            //store the path as text in the database
            $user->avatar = $image_name;
            $user->save();
        }

        return redirect()->back();
    }

    public function edit(User $user)
    {
        $user = Auth::user();

        return view('users.edit', compact('user'));
    }

    public function update(User $user)
    {
        if (Auth::user()->email == request('email')) {

            $this->validate(request(), [
                'name' => 'required',
                // 'email' => 'required|email|unique:users',
                'password' => 'required|min:4|confirmed'
            ]);


            $user->name = request('name');
            // $user->email = request('email');
            $user->password = md5(request('password'));

            $user->save();

            return back();
        } else { 

            $this->validate(request(), [
                'name' => 'required',
                'email' => 'email|required|unique:users',
                'password' => 'required|min:4|confirmed'
            ]);


            $user->name = request('name');
            $user->email = request('email');
            $user->password = md5(request('password'));

            $user->save();

            return back();
        }
    }
}
