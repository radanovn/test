@extends('layouts.app')

@section('content')

<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    View Post


    <a class="btn btn-primary" href="{{ route('posts.index') }}"> Back</a>


  </div>
  <div class="card-body">
    <div class="row">

      <div class="col-xs-12 col-sm-12 col-md-12"> 
    
      <div>
          <img class="rounded-circle" src="{{ asset('images/'.$post->image) }}" width="40px" height="40px" />
        </div>
        <br>

        <div class="form-group">


          <strong>Title:</strong>

          {{ $post -> title}}

        </div>

      </div>


      <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

          <strong>Body:</strong>

          {{ $post -> body }}

        </div>

      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">

        <div class="form-group">

          <strong>Comments</strong>

          @include('posts.commentsDisplay', ['comments' => $post->comments, 'post_id' => $post->id])

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

          <div class="form-group">

            <strong>Add comment</strong>

            <form method='POST' action="{{ route('comments.store') }}">
              @csrf
              <div class="form-group">
                <textarea class="form-control" name="body"></textarea>
                <input type="hidden" name='post_id' value="{{ $post->id }}">
              </div>
              <div class="form-group"></div>
              <input type="submit" class="btn btn-success" value="Add comment">
            </form>
          </div>

        </div>

      </div>

    </div>
  </div>
  @endsection